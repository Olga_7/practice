import re


class BinaryTree:
    def __init__(self):
        self.root = None
        self.precedence_order = {"+": 3, "-": 3, "*": 2, "/": 2, "^": 1}

    def __get_priority(self, lec):
        br, priority_indx, maximum = 0, 0, 0
        for i, s in enumerate(lec):
            if s.isdigit():
                continue
            if s == '(':
                br += 1
                continue
            if s == ')':
                br -= 1
                continue
            if s in self.precedence_order:
                priority = self.precedence_order[s] - 3 * br
                if priority >= maximum:
                    maximum = priority
                    priority_indx = i
        return priority_indx

    def __parser(self, string):
        if isinstance(string, str):
            string = string.replace(" ", "")
            array_lec = re.split('([()+-/*^])', string)
            array_lec = [x for x in array_lec if x]
            return array_lec

    def __build_tree(self, string, node):
        # if isinstance(string, str):
        #     string = self.__parser(string)
        not_operations = '()'
        priority_indx = self.__get_priority(string)
        # if node.is_leaf():
        #     node.value = float(string[priority_indx])
        #     return node.value

        if string[priority_indx] not in self.precedence_order:
            for char in not_operations:
                string = string.replace(char, '')
            node.value = float(string[priority_indx])
            return node.value
        else:
            node.value = string[priority_indx]
            node.left_child = BinaryTreeNode()
            node.right_child = BinaryTreeNode()
            node.left_child = self.__build_tree(string[:priority_indx], node.left_child)
            node.right_child = self.__build_tree(string[priority_indx + 1:], node.right_child)

    def __calcilate_tree(self, node):
        if node.value == "+":
            return node.left_child + node.right_child
        elif node.value == "-":
            return node.left_child - node.right_child
        elif node.value == "/":
            return node.left_child / node.right_child
        elif node.value == "*":
            return node.left_child * node.right_child
        else:
            return node.left_child ** node.right_child

    def solve(self, string):
        self.root = BinaryTreeNode()
        self.__build_tree(string, self.root)
        self.__calcilate_tree(self.root)


class BinaryTreeNode:
    def __init__(self):
        self.value = None
        self.right_child = None
        self.left_child = None

    def is_leaf(self):
        left_child_empty = self.left_child is None
        right_child_empty = self.right_child is None
        is_leaf = left_child_empty and right_child_empty
        return is_leaf
